# Neural net games
This is an AlphaGo Zero inspired project used for playing 
board games. Currently supports playing the Tic-tac-toe game on a board
with chosen size and with a chosen count of either crosses or noughts in
a row / column / diagonal needed to win.

The game supports three types of players:
 * Random player
 * A.I. player
 * Human player

## AlphaGo Zero similarities
 * No human knowledge, no past games' records nor any handcrafted heuristics
   for position evaluation are used. The program learns from scratch, playing
   against itself.
 * Uses the same Monte Carlo Trees expansion / exploration approach.
 * Plays a sample of all possible games, each one to it's end
   rather than exploring the game tree with breadth first search.
   This ensures the scalability of the approach to bigger boards.
 * Uses neural network to evaluate board situation and moves probabilities
   at the leafs of the game tree.

## AlphaGo Zero differences
 * Uses two neural networks, instead of one. The policy network
   predicts move probabilities. The evaluation network gives an overall
   score of the current board state.
 * The neural network architecture is simpler than the one used in the
   alpha-go-zero. Namely, instead of using residual neural network with
   convolutional layers, a basic multi-layered perceptron has been used.

## Results
Current algorithm can learn to play Tic-tac-toe on a standard 3x3 board in 
10 minutes and beat a Random player 80-10 (plus 10 draws) out of a 100 test games,
granted the first move.

## Project status
Currently the project consists of the game engine only, with text input / output
via console. The main focus now is to wrap it up in a web application format,
add simple GUI and deploy to the cloud, so it is available online, from the browser.

## Work in progress
The project is still quite young and not production ready. Code quality is OK-ish in
the architectural sense, but local code cleanups are necessary (marked by TODO tags).
More tests are necessary. The progress can be monitored by looking at the list of
issues created on GitLab.
