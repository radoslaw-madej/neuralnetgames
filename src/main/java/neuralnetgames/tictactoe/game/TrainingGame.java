package neuralnetgames.tictactoe.game;

import neuralnetgames.tictactoe.board.TicTacToeBoard;
import neuralnetgames.tictactoe.player.AIPlayer;

public class TrainingGame extends Game<AIPlayer> {

	public TrainingGame(int boardSize, int lineLengthToWin, AIPlayer crossPlayer, AIPlayer noughtPlayer) {
		super(boardSize, lineLengthToWin, crossPlayer, noughtPlayer);
	}

	@Override
	protected TicTacToeBoard nextMove(TicTacToeBoard board) {
		return currentPlayer.nextMoveStochastic(board);
	}

}
