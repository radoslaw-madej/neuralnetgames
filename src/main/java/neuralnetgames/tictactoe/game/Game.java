package neuralnetgames.tictactoe.game;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import neuralnetgames.tictactoe.board.TicTacToeBoard;
import neuralnetgames.tictactoe.board.TicTacToePawn;
import neuralnetgames.tictactoe.player.Player;

public abstract class Game<T extends Player> {

	private static final Logger logger = LoggerFactory.getLogger(Game.class);

	private final T crossPlayer;
	private final T noughtPlayer;

	protected T currentPlayer;
	private TicTacToeBoard board;


	public Game(int boardSize, int lineLengthToWin, T crossPlayer, T noughtPlayer) {
		this.crossPlayer = crossPlayer;
		this.noughtPlayer = noughtPlayer;
		this.currentPlayer = crossPlayer;
		this.board = TicTacToeBoard.emptyBoard(boardSize, lineLengthToWin);
	}

	protected abstract TicTacToeBoard nextMove(TicTacToeBoard board);

	public void play() {
		while(!hasFinished()) {
			board = nextMove(board);
			logger.debug(board.toString());
			switchPlayer();
		}
	}

	private void switchPlayer() {
		currentPlayer = currentPlayer.equals(crossPlayer) ? noughtPlayer : crossPlayer;
	}

	public Optional<T> getWinner() {
		if (!board.getWinner().isPresent()) {
			return Optional.empty();
		}
		TicTacToePawn winnerPawn = board.getWinner().get();
		Collection<T> players = new ArrayList<>();
		players.add(crossPlayer);
		players.add(noughtPlayer);
		for (T player : players) {
			if (player.getPawn() == winnerPawn) {
				return Optional.of(player);
			}
		}
		throw new IllegalStateException(format(
				"Winner pawn %s did not match any of the players: %s",
				winnerPawn, players));
	}

	private boolean hasFinished() {
		return board.isFinished();
	}

}
