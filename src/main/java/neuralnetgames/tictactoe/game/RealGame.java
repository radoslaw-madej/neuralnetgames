package neuralnetgames.tictactoe.game;

import neuralnetgames.tictactoe.board.TicTacToeBoard;
import neuralnetgames.tictactoe.player.Player;

public class RealGame<T extends Player> extends Game<T> {

	public RealGame(int boardSize, int lineLengthToWin, T crossPlayer, T noughtPlayer) {
		super(boardSize, lineLengthToWin, crossPlayer, noughtPlayer);
	}

	@Override
	protected TicTacToeBoard nextMove(TicTacToeBoard board) {
		return currentPlayer.nextMove(board);
	}

}
