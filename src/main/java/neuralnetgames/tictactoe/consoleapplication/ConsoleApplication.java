package neuralnetgames.tictactoe.consoleapplication;

import static com.google.common.collect.Maps.newHashMap;
import static neuralnetgames.tictactoe.board.TicTacToePawn.CROSS;
import static neuralnetgames.tictactoe.board.TicTacToePawn.NOUGHT;

import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import neuralnetgames.tictactoe.game.RealGame;
import neuralnetgames.tictactoe.player.AIPlayer;
import neuralnetgames.tictactoe.player.AIPlayerCoach;
import neuralnetgames.tictactoe.player.Player;
import neuralnetgames.tictactoe.player.RandomPlayer;

public class ConsoleApplication {

	private static final Logger logger = LoggerFactory.getLogger(ConsoleApplication.class);

	/**
	 * Random seed for development purposes is set in stone to provide reproducible results. In the long run the seed
	 * does not matter as the training games count should be high enough to eliminate much of a luck factor in learning.
	 */
	private static final int RANDOM_SEED = 19;
	private static final Random RANDOM = new Random(RANDOM_SEED);


	public static void main(String[] args) {
		int boardSize = 3;
		int lineLenghtToWin = 3;
		AIPlayer crossPlayer = new AIPlayer(CROSS, boardSize, lineLenghtToWin, RANDOM);
		Player noughtPlayer = new RandomPlayer(NOUGHT, RANDOM);
		AIPlayerCoach coach = new AIPlayerCoach(RANDOM);
		int trainingSessionsJump = 5;
		for (int j = 0; j < 1000; j += trainingSessionsJump) {
			Map<Optional<Player>, Integer> playerToWins = newHashMap();
			playerToWins.put(Optional.empty(), 0);
			playerToWins.put(Optional.of(crossPlayer), 0);
			playerToWins.put(Optional.of(noughtPlayer), 0);
			for (int i = 0; i < 20; i++) {
				logger.debug("game {}", i);
				RealGame<Player> game = new RealGame<Player>(boardSize, lineLenghtToWin, crossPlayer, noughtPlayer);
				game.play();
				playerToWins.put(game.getWinner(), playerToWins.get(game.getWinner()) + 1);
			}
			logger.info("trainingSessions: {} draws: {}, X: {}, O: {}",
					j,
					playerToWins.get(Optional.empty()),
					playerToWins.get(Optional.of(crossPlayer)),
					playerToWins.get(Optional.of(noughtPlayer)));

			crossPlayer = coach.train(crossPlayer, trainingSessionsJump, 10, 5);
		}
	}

}
