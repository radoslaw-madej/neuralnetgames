package neuralnetgames.tictactoe.board;

public enum TicTacToePawn implements Pawn {

	CROSS("X"), NOUGHT("O");

	private final String stringRepresentation;

	private TicTacToePawn(String stringRepresentation) {
		this.stringRepresentation = stringRepresentation;
	}

	@Override
	public String toString() {
		return stringRepresentation;
	}

}
