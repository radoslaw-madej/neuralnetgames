package neuralnetgames.tictactoe.board;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static neuralnetgames.tictactoe.board.Coordinates.coordinates;
import static neuralnetgames.tictactoe.board.TicTacToePawn.CROSS;
import static neuralnetgames.tictactoe.board.TicTacToePawn.NOUGHT;
import static neuralnetgames.tictactoe.board.SingleCoordinateIterator.Direction.CONSTANT;
import static neuralnetgames.tictactoe.board.SingleCoordinateIterator.Direction.DECREASE;
import static neuralnetgames.tictactoe.board.SingleCoordinateIterator.Direction.INCREASE;

import java.util.Collection;
import java.util.Optional;

import org.pcollections.HashPMap;
import org.pcollections.HashTreePMap;
import org.pcollections.HashTreePSet;
import org.pcollections.PSet;

import com.google.common.annotations.VisibleForTesting;

public class TicTacToeBoard {

	private static final int MIN_SIZE = 2;
	private static final int MIN_LINE_LENGTH_TO_WIN = 2;

	private final int size;
	private final int lineLenghtToWin;
	private final int hashCode;
	private final TicTacToePawn pawnToPlay;
	private final HashPMap<Coordinates, TicTacToePawn> board;

	private Optional<PSet<TicTacToeBoard>> availableMoves;
	private Optional<Coordinates> lastMove;
	private Optional<TicTacToePawn> winner;


	/**
	 * @see #emptyBoard(int, int)
	 */
	private TicTacToeBoard(int size, int lineLenghtToWin, HashPMap<Coordinates, TicTacToePawn> board, TicTacToePawn pawnToPlay,
			Coordinates lastMove, int hashCode) {

		checkConstructorArguments(size, lineLenghtToWin);
		this.size = size;
		this.lineLenghtToWin = lineLenghtToWin;
		this.board = board;
		this.pawnToPlay = pawnToPlay;
		this.hashCode = hashCode;
		this.availableMoves = Optional.empty();
		this.lastMove = Optional.ofNullable(lastMove);
		if (lastMove != null) {
			winner = calculateWinner();
		} else {
			winner = Optional.empty();
		}
	}

	public boolean isEmpty(int row, int column) {
		return !board.containsKey(coordinates(row, column));
	}

	public PSet<TicTacToeBoard> getAvailableMoves() {
		if (!availableMoves.isPresent()) {
			availableMoves = Optional.of(calculateAvailableMoves());
		}
		return availableMoves.get();
	}

	public Optional<Coordinates> getLastMove() {
		return lastMove;
	}

	public TicTacToePawn getPawnToPlay() {
		return pawnToPlay;
	}

	public Optional<TicTacToePawn> getWinner() {
		return winner;
	}

	public boolean isFinished() {
		return winner.isPresent() || board.size() == size * size;
	}

	public TicTacToePawn pawnAt(int row, int column) {
		return board.get(coordinates(row, column));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TicTacToeBoard other = (TicTacToeBoard) obj;
		if (board == null) {
			if (other.board != null) {
				return false;
			}
		} else if (!board.equals(other.board)) {
			return false;
		}
		if (lineLenghtToWin != other.lineLenghtToWin) {
			return false;
		}
		if (pawnToPlay != other.pawnToPlay) {
			return false;
		}
		if (size != other.size) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		return builder.append("\n")
				.append(boardToString())
				.append(format("Size: %s\nTo win: %s\nNext to play: %s\nWinner: %s",
						size, lineLenghtToWin, pawnToPlay, winner))
				.toString();
	}

	private String boardToString() {
		StringBuilder builder = new StringBuilder();
		for (int row = 0; row < size; row++) {
			for (int column = 0; column < size; column++) {
				Coordinates coordinates = coordinates(row, column);
				if (board.containsKey(coordinates)) {
					builder.append(board.get(coordinates));
				} else {
					builder.append(".");
				}
			}
			builder.append("\n");
		}
		return builder.toString();
	}

	@VisibleForTesting
	TicTacToeBoard play(TicTacToePawn pawn, int row, int column) {
		checkPlayArguments(pawn, row, column);
		TicTacToePawn newPawnToPlay = pawnToPlay == CROSS ? NOUGHT : CROSS;
		Coordinates coordinates = coordinates(row, column);
		int newHashCode = hashCode + calculateHashCodeIncrement(pawn, row, column);
		return new TicTacToeBoard(size, lineLenghtToWin, board.plus(coordinates, pawn), newPawnToPlay, coordinates, newHashCode);
	}

	private void checkPlayArguments(TicTacToePawn pawn, int row, int column) {
		checkArgument(row >= 0 && row < size,
				format("Row must be in range [%s, %s), but got: %s", 0, size, row));

		checkArgument(column >= 0 && column < size,
				format("Column must be in range [%s, %s), but got: %s", 0, size, column));

		checkArgument(isEmpty(row, column),
				format("Cannot play %s on [%s][%s]. Field already taken by %s.",
						pawn, row, column, pawnAt(row, column)));

		checkArgument(pawnToPlay == pawn,
				format("Cannot play %s. Current pawn to play: %s.", pawn, pawnToPlay));
	}

	private PSet<TicTacToeBoard> calculateAvailableMoves() {
		if (isFinished()) {
			return HashTreePSet.empty();
		}
		PSet<TicTacToeBoard> moves = HashTreePSet.empty();
		for (int row = 0; row < size; row++) {
			for (int column = 0; column < size; column++) {
				if (isEmpty(row, column)) {
					TicTacToeBoard move = play(pawnToPlay, row, column);
					moves = moves.plus(move);
				}
			}
		}
		return moves;
	}

	private Optional<TicTacToePawn> calculateWinner() {
		Coordinates lastMoveCoordinates = lastMove.get();
		Collection<CoordinatesIterator> iterators = newArrayList(
				// row
				new CoordinatesIterator(lineLenghtToWin - 1, size, lastMoveCoordinates, CONSTANT, INCREASE),
				// column
				new CoordinatesIterator(lineLenghtToWin - 1, size, lastMoveCoordinates, INCREASE, CONSTANT),
				// backslashDiagonal
				new CoordinatesIterator(lineLenghtToWin - 1, size, lastMoveCoordinates, INCREASE, INCREASE),
				// slashDiagonal
				new CoordinatesIterator(lineLenghtToWin - 1, size, lastMoveCoordinates, INCREASE, DECREASE));

		for (CoordinatesIterator iterator : iterators) {
			Optional<TicTacToePawn> winningPawn = checkWinner(iterator);
			if (winningPawn.isPresent()) {
				return winningPawn;
			}
		}
		return Optional.empty();
	}

	private Optional<TicTacToePawn> checkWinner(CoordinatesIterator forwardIterator) {
		int sum = 1;
		TicTacToePawn lastPawn = board.get(lastMove.get());
		while(forwardIterator.hasNext() && board.get(forwardIterator.next()) == lastPawn) {
			sum++;
		}
		CoordinatesIterator backwardIterator = forwardIterator.reverseIterator();
		while(backwardIterator.hasNext() && board.get(backwardIterator.next()) == lastPawn) {
			sum++;
		}
		if (sum >= lineLenghtToWin) {
			return Optional.of(lastPawn);
		} else {
			return Optional.empty();
		}
	}

	public static TicTacToeBoard emptyBoard(int size, int lineLenghtToWin) {
		TicTacToePawn initialPawnToPlay = CROSS;
		int initialHashCode = calculateInitialHashCode(size, lineLenghtToWin, initialPawnToPlay);
		return new TicTacToeBoard(size, lineLenghtToWin, HashTreePMap.empty(), initialPawnToPlay, null, initialHashCode);
	}

	/**
	 * This ensures that <tt>board1.equals(board2)</tt> implies that <tt>board1.hashCode()==board2.hashCode()</tt> for
	 * any two {@link TicTacToeBoard}s <tt>board1</tt> and <tt>board2</tt>, as required by the general contract of
	 * {@link Object#hashCode}.
	 *
	 * @see #calculateHashCodeIncrement(TicTacToePawn, int, int)
	 */
	private static int calculateInitialHashCode(int size, int lineLenghtToWin, TicTacToePawn pawnToPlay) {
		int prime = 31;
		int result = 1;
		result = prime * result + size;
		result = prime * result + lineLenghtToWin;
		result = prime * result + ((pawnToPlay == null) ? 0 : pawnToPlay.hashCode());
		return result;
	}

	/**
	 * This ensures that <tt>board1.equals(board2)</tt> implies that <tt>board1.hashCode()==board2.hashCode()</tt> for
	 * any two {@link TicTacToeBoard}s <tt>board1</tt> and <tt>board2</tt>, as required by the general contract of
	 * {@link Object#hashCode}.
	 *
	 * @see #calculateInitialHashCode(TicTacToePawn, int, int)
	 */
	private static int calculateHashCodeIncrement(TicTacToePawn pawn, int row, int column) {
		int prime = 31;
		int result = 1;
		result = prime * result + ((pawn == null) ? 0 : pawn.hashCode());
		result = prime * result + row;
		result = prime * result + column;
		return result;
	}

	private static void checkConstructorArguments(int size, int lineLengthToWin) {
		checkArgument(size >= MIN_SIZE, format("Size must be at least %s, but got %s.", MIN_SIZE, size));
		checkArgument(lineLengthToWin >= 2,
				format("Line length needed to win must be at least %s, but got %s.",
						MIN_LINE_LENGTH_TO_WIN, lineLengthToWin));

		checkArgument(lineLengthToWin <= size,
				format("Line length needed to win must be at most equal to size (%s), but got %s.",
						size, lineLengthToWin));
	}

}
