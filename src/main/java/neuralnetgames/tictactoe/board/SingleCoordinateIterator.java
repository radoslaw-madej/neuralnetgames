package neuralnetgames.tictactoe.board;

import static java.lang.String.format;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

public class SingleCoordinateIterator implements Iterator<Integer> {

	private final int startExclusive;
	private final int maxLength;
	private final int boardSize;
	private final Direction direction;

	private int current;


	public SingleCoordinateIterator(int startExclusive, int maxLength, int boardSize, Direction direction) {
		this.startExclusive = startExclusive;
		this.maxLength = maxLength;
		this.boardSize = boardSize;
		this.current = startExclusive;
		this.direction = direction;
	}

	@Override
	public boolean hasNext() {
		int next = direction.getCalculateNext().apply(current);
		return (next >= 0 && next < boardSize && Math.abs(next - startExclusive) <= maxLength);
	}

	@Override
	public Integer next() {
		if (!hasNext()) {
			throw new NoSuchElementException("no next element");
		}
		current = direction.getCalculateNext().apply(current);
		return current;
	}

	public static enum Direction {
		INCREASE(x -> x + 1),
		DECREASE(x -> x - 1),
		CONSTANT(x -> x);

		private final Function<Integer, Integer> calculateNext;

		private Direction(Function<Integer, Integer> calculateNext) {
			this.calculateNext = calculateNext;
		}

		public Function<Integer, Integer> getCalculateNext() {
			return calculateNext;
		}

		public Direction getReversed() {
			switch (this) {
			case INCREASE:
				return DECREASE;
			case DECREASE:
				return INCREASE;
			case CONSTANT:
				return CONSTANT;
			}
			throw new IllegalStateException(format("unknown enum constant: %s", this));
		}
	}

	public SingleCoordinateIterator reverse() {
		return new SingleCoordinateIterator(startExclusive, maxLength, boardSize, direction.getReversed());
	}

}
