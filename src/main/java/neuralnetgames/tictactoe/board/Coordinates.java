package neuralnetgames.tictactoe.board;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;

public class Coordinates {

	private final int row;
	private final int column;
	private final int hashCode;

	private Coordinates(int row, int column) {
		checkArguments(row, column);
		this.row = row;
		this.column = column;
		hashCode = calculateHashCode(row, column);
	}

	private void checkArguments(int row, int column) {
		checkArgument(row >= 0, "row can't be negative");
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Coordinates other = (Coordinates) obj;
		if (column != other.column) {
			return false;
		}
		if (row != other.row) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return format("[%s, %s]", row, column);
	}

	private static int calculateHashCode(int row, int column) {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	public static Coordinates coordinates(int row, int column) {
		return new Coordinates(row, column);
	}
}
