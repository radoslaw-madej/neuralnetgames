package neuralnetgames.tictactoe.board;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static neuralnetgames.tictactoe.util.Utils.arrayToArrayList;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import com.google.common.annotations.VisibleForTesting;

import neuralnetgames.tictactoe.neuralnet.NeuralNet;
import neuralnetgames.tictactoe.neuralnet.NeuralNetBuilder;

public class TicTacToeNetAdapter {

	private final NeuralNet policyNet;
	private final NeuralNet evaluationNet;
	private final int boardSize;
	private final int crossesOffset;
	private final int noughtsOffset;

	public TicTacToeNetAdapter(int boardSize, Random random) {
		this(boardSize, newRandomPolicyNet(boardSize, random), newRandomEvaulationNet(boardSize, random));
	}

	private TicTacToeNetAdapter(int boardSize, NeuralNet policyNet, NeuralNet evaluationNet) {
		this.boardSize = boardSize;
		crossesOffset = 0;
		noughtsOffset = boardSize * boardSize;
		this.policyNet = policyNet;
		this.evaluationNet = evaluationNet;
	}

	public double moveEvaluation(TicTacToeBoard move) {
		ArrayList<Double> outputSignal = evaluationNet.outputSignal(inputSignal(move));
		double winProbability = getOnlyElement(outputSignal);
		return neuronOutputToReward(winProbability);
	}

	public double moveProbability(TicTacToeBoard currentState, TicTacToeBoard move) {
		checkArgument(currentState.getAvailableMoves().contains(move),
				"illegal move: %s for state: %s",
				move, currentState);

		Optional<Coordinates> lastMoveCoordinates = move.getLastMove();
		checkState(lastMoveCoordinates.isPresent(), "no last move played");
		Coordinates coordinates = lastMoveCoordinates.get();
		int row = coordinates.getRow();
		int column = coordinates.getColumn();
		int outputSignalIndex = coordinatesToIdOutput(row, column, boardSize);
		ArrayList<Double> outputSignal = policyNet.outputSignal(inputSignal(currentState));
		return outputSignal.get(outputSignalIndex);
	}

	public void train(TicTacToeBoard board, Map<TicTacToeBoard, Double> expectedMoveToProbability, int expectedReward) {
		ArrayList<Double> inputSignal = inputSignal(board);

		ArrayList<Double> expectedOutputPolicySignal = expectedOutputPolicySignal(expectedMoveToProbability);
		policyNet.train(inputSignal, expectedOutputPolicySignal);

		ArrayList<Double> expectedOutputEvaluationSignal = expectedOutputEvaluationSignal(expectedReward);
		evaluationNet.train(inputSignal, expectedOutputEvaluationSignal);
	}

	public TicTacToeNetAdapter copy() {
		return new TicTacToeNetAdapter(boardSize, policyNet.copy(), evaluationNet.copy());
	}

	@VisibleForTesting
	ArrayList<Double> inputSignal(TicTacToeBoard move) {
		Double[] signal = new Double[2 * boardSize * boardSize + 1];
		for (int row = 0; row < boardSize; row++) {
			for (int column = 0; column < boardSize; column++) {
				if (move.isEmpty(row, column)) {
					signal[coordinatesToIdInput(row, column, boardSize, crossesOffset)] = 0.0;
					signal[coordinatesToIdInput(row, column, boardSize, noughtsOffset)] = 0.0;
					continue;
				}
				switch (move.pawnAt(row, column)) {
				case CROSS:
					signal[coordinatesToIdInput(row, column, boardSize, crossesOffset)] = 1.0;
					signal[coordinatesToIdInput(row, column, boardSize, noughtsOffset)] = 0.0;
					continue;
				case NOUGHT:
					signal[coordinatesToIdInput(row, column, boardSize, crossesOffset)] = 0.0;
					signal[coordinatesToIdInput(row, column, boardSize, noughtsOffset)] = 1.0;
					continue;
				}
			}
		}
		switch (move.getPawnToPlay()) {
		case CROSS:
			signal[2 * boardSize * boardSize] = 1.0;
			break;
		case NOUGHT:
			signal[2 * boardSize * boardSize] = 0.0;
			break;
		}
		return arrayToArrayList(signal);
	}

	@VisibleForTesting
	ArrayList<Double> expectedOutputPolicySignal(Map<TicTacToeBoard, Double> moveToProbability) {
		Double[] signal = new Double[boardSize * boardSize];
		zeroArray(signal);
		moveToProbability.forEach((board, probability) -> {
			Coordinates lastMove = board.getLastMove().get();
			int signalIndex = coordinatesToIdOutput(lastMove.getRow(), lastMove.getColumn(), boardSize);
			signal[signalIndex] = probability;
		});
		return arrayToArrayList(signal);
	}

	@VisibleForTesting
	ArrayList<Double> expectedOutputEvaluationSignal(int reward) {
		return newArrayList(rewardToNeuronOutput(reward));
	}

	/**
	 * @see #neuronOutputToReward(double)
	 */
	private static double rewardToNeuronOutput(double reward) {
		return (reward + 1.0) / 2.0;
	}

	/**
	 * @see #rewardToNeuronOutput(double)
	 */
	private static double neuronOutputToReward(double output) {
		return output * 2.0 - 1.0;
	}

	private static void zeroArray(Double[] signal) {
		for (int i = 0; i < signal.length; i++) {
			signal[i] = 0.0;
		}
	}

	private static NeuralNet newRandomPolicyNet(int boardSize, Random random) {
		NeuralNetBuilder builder = new NeuralNetBuilder(random);
		/*
		 * One neuron for each field for crosses, one for each field for noughts, one to signal which pawns play next.
		 */
		builder = builder.withInputLayer(2 * boardSize * boardSize + 1);
		/*
		 * One neuron for each field for crosses, one to signal probability for current pawns to play not to lose (i.e.
		 * to win or to draw).
		 */
		builder = builder.withOutputLayer(boardSize * boardSize);
		/*
		 * Some test values for hidden layers.
		 */
		builder = builder.withHiddenLayers(2 * boardSize * boardSize + 1 , 3);
		return builder.build();
	}

	private static NeuralNet newRandomEvaulationNet(int boardSize, Random random) {
		NeuralNetBuilder builder = new NeuralNetBuilder(random);
		/*
		 * One neuron for each field for crosses, one for each field for noughts, one to signal which pawns play next.
		 */
		builder = builder.withInputLayer(2 * boardSize * boardSize + 1);
		/*
		 * One neuron for each field for crosses, one to signal probability for current pawns to play not to lose (i.e.
		 * to win or to draw).
		 */
		builder = builder.withOutputLayer(1);
		/*
		 * Some test values for hidden layers.
		 */
		builder = builder.withHiddenLayers(2 * boardSize * boardSize + 1, 3);
		return builder.build();
	}

	private static int coordinatesToIdOutput(int row, int column, int boardSize) {
		return coordinatesToIdInput(row, column, boardSize, 0);
	}

	private static int coordinatesToIdInput(int row, int column, int boardSize, int offset) {
		return row * boardSize + column + offset;
	}

}
