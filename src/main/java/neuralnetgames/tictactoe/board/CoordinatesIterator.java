package neuralnetgames.tictactoe.board;

import static neuralnetgames.tictactoe.board.Coordinates.coordinates;

import java.util.Iterator;
import java.util.NoSuchElementException;

import neuralnetgames.tictactoe.board.SingleCoordinateIterator.Direction;

public class CoordinatesIterator implements Iterator<Coordinates>{

	private final SingleCoordinateIterator rowIterator;
	private final SingleCoordinateIterator columnIterator;

	public CoordinatesIterator(int maxLength, int boardSize, Coordinates startCoordinatesExclusive,
			Direction rowDirection, Direction columnDirection) {

		this(new SingleCoordinateIterator(startCoordinatesExclusive.getRow(), maxLength, boardSize, rowDirection),
				new SingleCoordinateIterator(startCoordinatesExclusive.getColumn(), maxLength, boardSize, columnDirection));
	}

	private CoordinatesIterator(SingleCoordinateIterator rowIterator, SingleCoordinateIterator columnIterator) {
		this.rowIterator = rowIterator;
		this.columnIterator = columnIterator;
	}

	@Override
	public boolean hasNext() {
		return rowIterator.hasNext() && columnIterator.hasNext();
	}

	@Override
	public Coordinates next() {
		if (!hasNext()) {
			throw new NoSuchElementException("no next element");
		}
		return coordinates(rowIterator.next(), columnIterator.next());
	}

	public CoordinatesIterator reverseIterator() {
		return new CoordinatesIterator(rowIterator.reverse(), columnIterator.reverse());
	}

}
