package neuralnetgames.tictactoe.util;

import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import neuralnetgames.tictactoe.board.TicTacToeBoard;

public final class Utils {

	private Utils() {
		throw new UnsupportedOperationException("a util class");
	}

	/**
	 * TODO Well, this is ugly. If possible, rewrite the {@link #inputSignal(TicTacToeBoard)} to use
	 * {@link ArrayList<Double>} only, without an explicit array. The problem is, I don't think there is a way to create
	 * an empty {@link ArrayList} with expected size already physically reserved. The
	 * {@link Lists#newArrayListWithExpectedSize(int)} doesn't work this way either. As a result, trying to use
	 * {@link ArrayList#add(int, Object)} with index greater than the current {@link ArrayList#size()} throws an
	 * {@link IndexOutOfBoundsException}. Cannot even use stream collectors here, because the only partially suitable
	 * one returns a {@link List}, and there is none returning an {@link ArrayList}...
	 */
	public static <T> ArrayList<T> arrayToArrayList(T[] signal) {
		ArrayList<T> result = newArrayList();
		for (int i = 0; i < signal.length; i++) {
			result.add(signal[i]);
		}
		return result;
	}

}
