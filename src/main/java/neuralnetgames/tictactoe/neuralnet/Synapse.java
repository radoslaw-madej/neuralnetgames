package neuralnetgames.tictactoe.neuralnet;

import static java.lang.String.format;

import java.util.Random;

public class Synapse {

	private final Neuron source;
	private final ActiveNeuron target;

	private double weight;


	/**
	 * @see #newSynapseWithRandomWeight(Neuron, Neuron, Random)
	 */
	public Synapse(Neuron source, ActiveNeuron target, double weight) {
		this.source = source;
		this.target = target;
		this.weight = weight;
	}

	public double getSignal() {
		return weight * source.getOutputSignal();
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Neuron getSource() {
		return source;
	}

	public ActiveNeuron getTarget() {
		return target;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Synapse other = (Synapse) obj;
		if (source == null) {
			if (other.source != null) {
				return false;
			}
		} else if (!source.equals(other.source)) {
			return false;
		}
		if (target == null) {
			if (other.target != null) {
				return false;
			}
		} else if (!target.equals(other.target)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return format("%s --(%.2f)-> %s", source, weight, target);
	}

	public static Synapse newSynapseWithRandomWeight(Neuron source, ActiveNeuron target, Random random) {
		return new Synapse(source, target, random.nextDouble() / 10.0 - 1.0 / 20.0);
	}

	public Synapse copy() {
		throw new UnsupportedOperationException("not implemented yet");
	}

}
