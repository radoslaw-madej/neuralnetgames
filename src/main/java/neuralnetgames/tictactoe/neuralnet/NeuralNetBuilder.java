package neuralnetgames.tictactoe.neuralnet;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;
import static neuralnetgames.tictactoe.neuralnet.Synapse.newSynapseWithRandomWeight;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;

public class NeuralNetBuilder {

	private final Random random;
	private final IdProvider neuronIdProvider;

	private Optional<NeuralNet> neuralNet;
	private Optional<Integer> neuronsCountInInputLayer;
	private Optional<Integer> neuronsCountInOutputLayer;
	private Optional<Integer> neuronsCountInHiddenLayer;
	private Optional<Integer> hiddenLayersCount;

	public NeuralNetBuilder(Random random) {
		this.random = random;
		neuronIdProvider = new IdProvider();
		neuralNet = Optional.empty();
		neuronsCountInInputLayer = Optional.empty();
		neuronsCountInOutputLayer = Optional.empty();
		neuronsCountInHiddenLayer = Optional.empty();
		hiddenLayersCount = Optional.empty();
	}

	public NeuralNetBuilder withInputLayer(int neuronsCountInInputLayer) {
		checkArgument(neuronsCountInInputLayer > 0, "neuronsCountInInputLayer must be a positive integer");
		this.neuronsCountInInputLayer = Optional.of(neuronsCountInInputLayer);
		return this;
	}

	public NeuralNetBuilder withOutputLayer(int neuronsCountInOutputLayer) {
		checkArgument(neuronsCountInOutputLayer > 0, "neuronsCountInOutputLayer must be a positive integer");
		this.neuronsCountInOutputLayer = Optional.of(neuronsCountInOutputLayer);
		return this;
	}

	public NeuralNetBuilder withHiddenLayers(int neuronsCountInHiddenLayers, int hiddenLayersCount) {
		checkArgument(neuronsCountInHiddenLayers > 0, "neuronsCountInHiddenLayers must be a positive integer");
		checkArgument(neuronsCountInHiddenLayers > 0, "hiddenLayersCount must be a positive integer");
		this.neuronsCountInHiddenLayer = Optional.of(neuronsCountInHiddenLayers);
		this.hiddenLayersCount = Optional.of(hiddenLayersCount);
		return this;
	}

	public NeuralNet build() {
		checkRequiredParameters();
		if (!neuralNet.isPresent()) {
			buildNeuralNet();
		}
		return neuralNet.get();
	}

	// TODO make this a static method returning built neural net
	private void buildNeuralNet() {
		ArrayList<ActiveNeuron> outputLayer = buildActiveLayer(neuronsCountInOutputLayer.get(), neuronIdProvider);
		ArrayList<ActiveNeuron> deepestActiveLayer = outputLayer;
		for (int i = 0; i < hiddenLayersCount.get(); i++) {
			ArrayList<ActiveNeuron> hiddenLayer = buildActiveLayer(neuronsCountInHiddenLayer.get(), neuronIdProvider);
			connect(hiddenLayer, deepestActiveLayer, random);
			deepestActiveLayer = hiddenLayer;
		}
		ArrayList<PassiveNeuron> inputLayer = buildPassiveLayer(neuronsCountInInputLayer.get(), neuronIdProvider);
		connect(inputLayer, deepestActiveLayer, random);
		neuralNet = Optional.of(new NeuralNet(inputLayer, outputLayer));
	}

	private static ArrayList<ActiveNeuron> buildActiveLayer(int neuronsCount, IdProvider neuronIdProvider) {
		return buildLayer(neuronsCount, neuronIdProvider, ActiveNeuron::new);
	}

	private static ArrayList<PassiveNeuron> buildPassiveLayer(int neuronsCount, IdProvider neuronIdProvider) {
		return buildLayer(neuronsCount, neuronIdProvider, PassiveNeuron::new);
	}

	private static <N extends Neuron> ArrayList<N> buildLayer(int neuronsCount, IdProvider neuronIdProvider,
			Function<Integer, N> neuronConstructor) {

		ArrayList<N> layer = newArrayList();
		for (int i = 0; i < neuronsCount; i++) {
			N neuron = neuronConstructor.apply(neuronIdProvider.nextId());
			layer.add(neuron);
		}
		return layer;
	}

	private static <N extends Neuron> void connect(ArrayList<N> sourceLayer, ArrayList<ActiveNeuron> targetLayer,
			Random random) {

		targetLayer.forEach(targetNeuron -> {
			sourceLayer.forEach(sourceNeuron -> {
				Synapse synapse = newSynapseWithRandomWeight(sourceNeuron, targetNeuron, random);
				targetNeuron.addInSynapse(synapse);
				sourceNeuron.addOutSynapse(synapse);
			});
		});
	}

	private void checkRequiredParameters() {
		checkArgument(neuronsCountInInputLayer.isPresent(), "neurons count in input layer not set");
		checkArgument(neuronsCountInOutputLayer.isPresent(), "neurons count in output layer not set");
		checkArgument(neuronsCountInHiddenLayer.isPresent(), "neurons count in hidden layers not set");
		checkArgument(hiddenLayersCount.isPresent(), "hidden layers count not set");
	}

	private static class IdProvider {
		int id = 0;

		int nextId() {
			return id++;
		}
	}

}
