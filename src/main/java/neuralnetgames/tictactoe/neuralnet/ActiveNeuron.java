package neuralnetgames.tictactoe.neuralnet;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.exp;
import static java.lang.Math.pow;

import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

public class ActiveNeuron extends Neuron {

	/**
	 * This is tightly coupled with {@link #ACTIVATION_FUNCTION_DERIVATIVE}. Do not change one without changing the
	 * other.
	 */
	private static final Function<Double, Double> ACTIVATION_FUNCTION = logisticFunction();

	/**
	 * This is tightly coupled with {@link #ACTIVATION_FUNCTION}. Do not change one without changing the other.
	 */
	private static final Function<Double, Double> ACTIVATION_FUNCTION_DERIVATIVE = logisticFunctionDerivative();

	private Optional<Double> weightedInputSignalsSum;


	public ActiveNeuron(int id) {
		super(id);
		weightedInputSignalsSum = Optional.empty();
	}

	@Override
	public double getOutputSignal() {
		if (!weightedInputSignalsSum.isPresent()) {
			weightedInputSignalsSum = Optional.of(calculateWeightedInputSignalsSum(inSynapses));
		}
		return ACTIVATION_FUNCTION.apply(weightedInputSignalsSum.get());
	}

	public double getOutputSignalDerivative() {
		if (!weightedInputSignalsSum.isPresent()) {
			weightedInputSignalsSum = Optional.of(calculateWeightedInputSignalsSum(inSynapses));
		}
		return ACTIVATION_FUNCTION_DERIVATIVE.apply(weightedInputSignalsSum.get());
	}

	@Override
	public void clear() {
		weightedInputSignalsSum = Optional.empty();
	}

	public void addInSynapse(Synapse inSynapse) {
		checkArgument(
				inSynapse.getTarget() == this,
				"Incorrect target node found in the inSynapse. Expected: %s, but got: %s",
				this, inSynapse.getTarget());

		inSynapses.add(inSynapse);
	}

	@Override
	@SuppressWarnings("unchecked")
	public ActiveNeuron copyWithoutSynapses() {
		return new ActiveNeuron(id);
	}

	private static double calculateWeightedInputSignalsSum(Set<Synapse> inSynapses) {
		return inSynapses
				.stream()
				.mapToDouble(Synapse::getSignal)
				.sum();
	}

	/**
	 * This is tightly coupled with {@link #logisticFunctionDerivative()}. Do not change one without changing other.
	 */
	private static Function<Double, Double> logisticFunction() {
		return x -> 1 / (1 + exp(-x));
	}

	/**
	 * This is tightly coupled with {@link #logisticFunction()}. Do not change one without changing other.
	 */
	private static Function<Double, Double> logisticFunctionDerivative() {
		return x -> exp(-x) / pow((1 + exp(-x)), 2);
	}

}
