package neuralnetgames.tictactoe.neuralnet;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;

import java.util.Set;

public abstract class Neuron {

	protected final int id;

	protected Set<Synapse> inSynapses;
	protected Set<Synapse> outSynapses;

	protected Neuron(int id) {
		this.id = id;
		inSynapses = newHashSet();
		outSynapses = newHashSet();
	}

	public abstract double getOutputSignal();

	public abstract void clear();

	public int getId() {
		return id;
	}

	public Set<Synapse> getInSynapses() {
		return inSynapses;
	}

	public Set<Synapse> getOutSynapses() {
		return outSynapses;
	}

	public void addOutSynapse(Synapse outSynapse) {
		checkArgument(
				outSynapse.getSource() == this,
				"Incorrect sorce node found in the outSynapse. Expected: %s, but got: %s",
				this, outSynapse.getSource());

		outSynapses.add(outSynapse);
	}

	public abstract <N extends Neuron> N copyWithoutSynapses();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Neuron other = (Neuron) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return format("%s", id);
	}

}
