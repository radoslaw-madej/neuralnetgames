package neuralnetgames.tictactoe.neuralnet;

import static java.lang.String.format;

import java.util.Optional;

public class PassiveNeuron extends Neuron {

	private Optional<Double> outputSignal;

	public PassiveNeuron(int id) {
		super(id);
		outputSignal = Optional.empty();
	}

	@Override
	public double getOutputSignal() {
		if (!outputSignal.isPresent()) {
			throw new IllegalStateException(format("outputSignal not set for a passive neuron: %s", this));
		}
		return outputSignal.get();
	}

	public void setOutputSignal(double outputSignal) {
		this.outputSignal = Optional.of(outputSignal);
	}

	@Override
	public void clear() {
		outputSignal = Optional.empty();
	}

	@Override
	@SuppressWarnings("unchecked")
	public PassiveNeuron copyWithoutSynapses() {
		return new PassiveNeuron(id);
	}

}
