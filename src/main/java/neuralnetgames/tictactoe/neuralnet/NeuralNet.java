package neuralnetgames.tictactoe.neuralnet;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;
import static neuralnetgames.tictactoe.util.Utils.arrayToArrayList;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.BiFunction;

public class NeuralNet {

	private static final double DEFAULT_LEARINING_RATE = 1;
	private static final BiFunction<ArrayList<Double>, ArrayList<Double>, Double> COST_FUNCTION = squaredErrorHalved();

	private ArrayList<PassiveNeuron> inputLayer;
	private ArrayList<ActiveNeuron> outputLayer;

	/**
	 * Maps a synapse to the cost function derivative. The derivative is taken with respect to this synapse weight.
	 */
	private Map<Synapse, Double> synapseToCostDerivative;
	private Map<ActiveNeuron, Double> neuronToTargetMultiplier;
	private Map<ActiveNeuron, Double> outputNeuronToExpectedSignal;

	/**
	 * TODO Can this be done more elegantly, without explicitly passing the input layer?
	 */
	public NeuralNet(ArrayList<PassiveNeuron> inputLayer, ArrayList<ActiveNeuron> outputLayer) {
		this.inputLayer = inputLayer;
		this.outputLayer = outputLayer;
		synapseToCostDerivative = newHashMap();
		neuronToTargetMultiplier = newHashMap();
		outputNeuronToExpectedSignal = newHashMap(); // TODO remove
	}

	/**
	 * The ArrayList has been chosen as an input / output format rather than an array or simply a List. Arrays don't
	 * play well with generics and newer java idioms (e.g. streams). The List interface, on the other hand, does not
	 * guarantee a constant time cost for accessing elements by index.
	 *
	 * This is a general purpose neural net. Mapping the input / output to a desired business logic is left to the user
	 * of the {@link NeuralNet}.
	 *
	 * @param inputSignal
	 * @return outputSignal
	 */
	public ArrayList<Double> outputSignal(ArrayList<Double> inputSignal) {
		clearNeuralNet();
		setInputLayerSignal(inputSignal);
		return calculateOutputSignal();
	}

	/**
	 * @see #train(ArrayList, ArrayList, double)
	 */
	public void train(ArrayList<Double> inputSignal, ArrayList<Double> expectedOutputSignal) {
		train(inputSignal, expectedOutputSignal, DEFAULT_LEARINING_RATE);
	}

	/**
	 * Training is done according to: https://en.wikipedia.org/wiki/Backpropagation#Derivation
	 */
	public void train(ArrayList<Double> inputSignal, ArrayList<Double> expectedOutputSignal, double learningRate) {
		clearNeuralNet();
		setInputLayerSignal(inputSignal);
		for (int i = 0; i < expectedOutputSignal.size(); i++) {
			this.outputNeuronToExpectedSignal.put(outputLayer.get(i), expectedOutputSignal.get(i));
		}
		outputLayer.forEach(this::collectWeightsGradient);
		adjustWeights(synapseToCostDerivative, learningRate);
	}

	public NeuralNet copy() {
		Map<Neuron, Set<Synapse>> neuronToOutSynapses = traverseNet(this);
		Map<Neuron, Neuron> neuronToNeuronCopy = copy(neuronToOutSynapses);
		ArrayList<PassiveNeuron> inputLayerCopy = inputLayerCopy(neuronToNeuronCopy);
		ArrayList<ActiveNeuron> outputLayerCopy = outputLayerCopy(neuronToNeuronCopy);
		return new NeuralNet(inputLayerCopy, outputLayerCopy);
	}

	private ArrayList<PassiveNeuron> inputLayerCopy(Map<Neuron, Neuron> neuronToneuronCopy) {
		PassiveNeuron[] inputLayerCopyArray = new PassiveNeuron[inputLayer.size()];
		for (int i = 0; i < inputLayer.size(); i++) {
			PassiveNeuron originalNeuron = inputLayer.get(i);
			PassiveNeuron neuronCopy = (PassiveNeuron) neuronToneuronCopy.get(originalNeuron);
			checkNotNull(neuronCopy, "no neuron copy found for neuron: %s", originalNeuron);
			inputLayerCopyArray[i] = neuronCopy;
		}
		ArrayList<PassiveNeuron> inputLayerCopy = arrayToArrayList(inputLayerCopyArray);
		return inputLayerCopy;
	}

	private ArrayList<ActiveNeuron> outputLayerCopy(Map<Neuron, Neuron> neuronToNeuronCopy) {
		ActiveNeuron[] outputLayerCopyArray = new ActiveNeuron[outputLayer.size()];
		for (int i = 0; i < outputLayer.size(); i++) {
			ActiveNeuron originalNeuron = outputLayer.get(i);
			ActiveNeuron neuronCopy = (ActiveNeuron) neuronToNeuronCopy.get(originalNeuron);
			checkNotNull(neuronCopy, "no neuron copy found for neuron: %s", originalNeuron);
			outputLayerCopyArray[i] = neuronCopy;
		}
		ArrayList<ActiveNeuron> outputLayerCopy = arrayToArrayList(outputLayerCopyArray);
		return outputLayerCopy;
	}

	private void collectWeightsGradient(ActiveNeuron activeNeuron) {
		double targetMultiplier = outerLayerDelta(activeNeuron);
		activeNeuron.getInSynapses()
				.stream()
				.filter(synapse -> !synapseToCostDerivative.containsKey(synapse))
				.forEach(synapse -> {
					double sourceMultiplicand = synapse.getSource().getOutputSignal();
					double weightDerivative = sourceMultiplicand * targetMultiplier;
					synapseToCostDerivative.put(synapse, weightDerivative);
					if (!inputLayer.contains(synapse.getSource())) { // source is an active neuron, needs teaching
						// TODO can this be done nicer, without casting?
						collectWeightsGradient((ActiveNeuron) synapse.getSource());
					}
				});
	}

	private double outerLayerDelta(ActiveNeuron activeNeuron) {
		return neuronToTargetMultiplier.computeIfAbsent(activeNeuron, n -> {
			if (outputNeuronToExpectedSignal.containsKey(n)) { // neuron in the output layer
				double difference = n.getOutputSignal() - outputNeuronToExpectedSignal.get(n);
				return difference * n.getOutputSignalDerivative();
			} else { // neuron in the hidden layer
				double sum = n.getOutSynapses()
						.stream()
						.mapToDouble(synapse -> {
							ActiveNeuron targetNeuron = synapse.getTarget();
							double targetMultiplier = outerLayerDelta(targetNeuron);
							double weight = synapse.getWeight();
							return targetMultiplier * weight;
						})
						.sum();

				return sum * n.getOutputSignalDerivative();
			}
		});
	}

	private static void adjustWeights(Map<Synapse, Double> synapseToCostDerivative, double learningRate) {
		synapseToCostDerivative.forEach( (synapse, weightGradient) -> {
				synapse.setWeight(synapse.getWeight() - learningRate * weightGradient);
				});
	}

	private void clearNeuralNet() {
		synapseToCostDerivative = newHashMap();
		this.outputNeuronToExpectedSignal = newHashMap(); // TODO remove
		this.neuronToTargetMultiplier = newHashMap();
		Set<Neuron> visitedNeurons = newHashSet();
		for (Neuron neuron : outputLayer) {
			clear(neuron, visitedNeurons);
		}
	}

	private static void clear(Neuron neuron, Set<Neuron> visited) {
		if (!visited.contains(neuron)) {
			visited.add(neuron);
			neuron.clear();
			neuron.getInSynapses()
					.stream()
					.map(Synapse::getSource)
					.forEach(source -> clear(source, visited));
		}
	}

	private ArrayList<Double> calculateOutputSignal() {
		ArrayList<Double> result = newArrayList();
		for (int i = 0; i < outputLayer.size(); i++) {
			result.add(outputLayer.get(i).getOutputSignal());
		}
		return result;
	}

	private void setInputLayerSignal(ArrayList<Double> inputSignal) {
		for (int i = 0; i < inputLayer.size(); i++) {
			inputLayer.get(i).setOutputSignal(inputSignal.get(i));
		}
	}

	private static Map<Neuron, Set<Synapse>> traverseNet(NeuralNet neuralNet) {
		Map<Neuron, Set<Synapse>> accumulator = newHashMap();
		for (Neuron neuron : neuralNet.inputLayer) {
			traverse(neuron, neuralNet, accumulator);
		}
		return accumulator;
	}

	private static void traverse(Neuron neuron, NeuralNet neuralNet, Map<Neuron, Set<Synapse>> accumulator) {
		if (accumulator.containsKey(neuron)) {
			return;
		}
		Set<Synapse> outSynapses = neuron.getOutSynapses();
		accumulator.put(neuron, outSynapses);
		outSynapses
				.stream()
				.map(Synapse::getTarget)
				.forEach(outNeuron -> traverse(outNeuron, neuralNet, accumulator));
	}

	private static Map<Neuron, Neuron> copy(Map<Neuron, Set<Synapse>> neuronToOutSynapses) {
		Map<Neuron, Neuron> neuronToNeuronCopy = newHashMap();

		// copy neurons
		neuronToOutSynapses
				.keySet()
				.forEach(neuron -> neuronToNeuronCopy.put(neuron, neuron.copyWithoutSynapses()));

		// add synapse copies
		neuronToOutSynapses
				.values()
				.stream()
				.flatMap(Set::stream)
				.forEach(synapse -> {
					Neuron sourceNeuronCopy = neuronToNeuronCopy.get(synapse.getSource());
					ActiveNeuron targetNeuronCopy = (ActiveNeuron) neuronToNeuronCopy.get(synapse.getTarget());
					Synapse synapseCopy = new Synapse(sourceNeuronCopy, targetNeuronCopy, synapse.getWeight());
					sourceNeuronCopy.addOutSynapse(synapseCopy);
					targetNeuronCopy.addInSynapse(synapseCopy);
				});
		return neuronToNeuronCopy;
	}

	/**
	 * Halving the squared error is for convenience only (backpropagation formulas look simpler). In the end it does not
	 * matter, because there is a learning constant (a constant multiplier), chosen arbitrarily.
	 */
	private static BiFunction<ArrayList<Double>, ArrayList<Double>, Double> squaredErrorHalved() {
		return (actual, expected) -> {
			checkArgument(actual.size() == expected.size(),
					"actual signal size (got %s) should be equal to the expected signal size (got %s).",
					actual.size(), expected.size());

			int size = actual.size();
			double sumOfSquares = 0.0;
			for (int i = 0; i < size; i++) {
				sumOfSquares += Math.pow((actual.get(i) - expected.get(i)), 2);
			}
			return 0.5 * sumOfSquares;
		};
	}

	public static void main(String[] args) {
		Random random = new Random(17);
		NeuralNetBuilder builder = new NeuralNetBuilder(random);
		NeuralNet net = builder.withInputLayer(4)
				.withOutputLayer(2)
				.withHiddenLayers(3, 2)
				.build();

		ArrayList<Double> inputSignal = newArrayList(1.0, 0.4, 0.0, -0.5);
		ArrayList<Double> expectedOutputSignal = newArrayList(0.0, 1.0);
		ArrayList<Double> actualOutputSignal = net.outputSignal(inputSignal);
		for (int i = 0; i < 100; i++) {
			net.train(inputSignal, expectedOutputSignal);
		}
		ArrayList<Double> trainedActualOutputSignal = net.outputSignal(inputSignal);

		StringBuilder consoleOutput = new StringBuilder();
		consoleOutput.append(format("inputSignal: %s", inputSignal))
				.append("\n")
				.append(format("actualOutputSignal: %s", actualOutputSignal))
				.append("\n")
				.append(format("expectedOutputSignal: %s", expectedOutputSignal))
				.append("\n")
				.append(format("trainedOutputSignal:  %s", trainedActualOutputSignal))
				.append("\n");

		System.out.println(consoleOutput.toString());
	}

}
