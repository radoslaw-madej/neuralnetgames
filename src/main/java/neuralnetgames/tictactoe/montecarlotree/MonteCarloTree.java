package neuralnetgames.tictactoe.montecarlotree;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Optional;
import java.util.Set;

import neuralnetgames.tictactoe.board.TicTacToeBoard;

public class MonteCarloTree {

	private final TicTacToeBoard board;
	private final Optional<MonteCarloTree> parent;
	private final double predictedProbability;

	private int games;
	private double treeValue;
	private Set<MonteCarloTree> children;

	public MonteCarloTree(TicTacToeBoard board, double predictedProbability) {
		this(board, predictedProbability, Optional.empty());
	}

	public MonteCarloTree(TicTacToeBoard board, double predictedProbability, MonteCarloTree parent) {
		this(board, predictedProbability, Optional.of(parent));
	}

	private MonteCarloTree(TicTacToeBoard board, double predictedProbability, Optional<MonteCarloTree> parent) {
		this.board = board;
		this.parent = parent;
		this.predictedProbability = predictedProbability;
		games = 0;
		treeValue = 0.0;
		children = newHashSet();
	}

	public double meanTreeValue() {
		checkState(games > 0, "cannot divide by zero (games)");
		return (double) getTreeValue() / (double) getGames();
	}

	public void addChild(TicTacToeBoard board, double predictedProbability) {
		children.add(new MonteCarloTree(board, predictedProbability, this));
	}

	public Optional<MonteCarloTree> getParent() {
		return parent;
	}

	public TicTacToeBoard getBoard() {
		return this.board;
	}

	public int getGames() {
		return games;
	}

	public double getTreeValue() {
		return treeValue;
	}

	public double getPredictedProbability() {
		return predictedProbability;
	}

	public void increaseTreeValue(double addendValue) {
		games += 1;
		this.treeValue += addendValue;
	}

	public Set<MonteCarloTree> getChildren() {
		return children;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MonteCarloTree other = (MonteCarloTree) obj;
		if (board == null) {
			if (other.board != null) {
				return false;
			}
		} else if (!board.equals(other.board)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("\n")
				.append(board)
				.append("\n")
				.append("games: ")
				.append(games)
				.append("\n")
				.append("tree value: ")
				.append(treeValue)
				.append("\n")
				.append("predicted probability: ")
				.append(predictedProbability)
				.append("\n");

		if (games != 0) {
			result.append("mean tree value: ")
					.append(meanTreeValue())
					.append("\n");
		}
		return result.toString();
	}

}
