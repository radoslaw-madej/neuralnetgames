package neuralnetgames.tictactoe.player;

import static com.google.common.collect.Maps.newHashMap;
import static neuralnetgames.tictactoe.board.TicTacToePawn.CROSS;
import static neuralnetgames.tictactoe.board.TicTacToePawn.NOUGHT;
import static neuralnetgames.tictactoe.player.Reward.DRAW;
import static neuralnetgames.tictactoe.player.Reward.LOSS;
import static neuralnetgames.tictactoe.player.Reward.WIN;

import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import neuralnetgames.tictactoe.board.TicTacToePawn;
import neuralnetgames.tictactoe.game.TrainingGame;

public class AIPlayerCoach {

	private static final Logger logger = LoggerFactory.getLogger(AIPlayer.class);
	private static final int EVALUATION_GAMES_COUNT = 10;

	private final Random random;

	public AIPlayerCoach(Random random) {
		this.random = random;
	}

	public AIPlayer train(AIPlayer player, int trainingSessions, int gamesPerTrainingSession, int gamesSample) {
		AIPlayer candidate = player.copy();
		for (int i = 0; i < trainingSessions; i++) {
			logger.debug("internal training session: {}", i);
			candidate.trainOnce(gamesPerTrainingSession, gamesSample);
		}
		return chooseBetter(player, candidate);
	}

	/**
	 * TODO The usage of player names here as well as this whole method looks ugly... Refactor.
	 */
	private AIPlayer chooseBetter(AIPlayer current, AIPlayer candidate) {
		int boardSize = current.getBoardSize();
		int lineLengthToWin = current.getLineLengthToWin();

		String currentPlayerName = "current";
		String candidatePlayerName = "candidate";

		AIPlayer currentCrossPlayer = current.copy(CROSS, currentPlayerName);
		AIPlayer currentNoughtPlayer = current.copy(NOUGHT, currentPlayerName);
		AIPlayer candidateCrossPlayer = candidate.copy(CROSS, candidatePlayerName);
		AIPlayer candidateNoughtPlayer = candidate.copy(NOUGHT, candidatePlayerName);

		Map<String, Integer> playerNameToScore = newHashMap();
		playerNameToScore.put(candidatePlayerName, 0);
		playerNameToScore.put(currentPlayerName, 0);
		for (int i = 0; i < EVALUATION_GAMES_COUNT; i++) {
			AIPlayer crossPlayer;
			AIPlayer noughtPlayer;
			if (random.nextBoolean()) {
				crossPlayer = currentCrossPlayer;
				noughtPlayer = candidateNoughtPlayer;
			} else {
				crossPlayer = candidateCrossPlayer;
				noughtPlayer = currentNoughtPlayer;
			}
			TrainingGame game = new TrainingGame(boardSize, lineLengthToWin, crossPlayer, noughtPlayer);
			game.play();
			if(game.getWinner().isPresent()) {
				TicTacToePawn winnerPawn = game.getWinner().get().getPawn();
				String winnerName = winnerPawn == CROSS ? crossPlayer.getName() : noughtPlayer.getName();
				String loserName = winnerPawn == CROSS ? noughtPlayer.getName() : crossPlayer.getName();
				playerNameToScore.put(winnerName, playerNameToScore.get(winnerName) + WIN.getScore());
				playerNameToScore.put(loserName, playerNameToScore.get(loserName) + LOSS.getScore());
			} else {
				playerNameToScore.put(currentPlayerName, playerNameToScore.get(currentPlayerName) + DRAW.getScore());
				playerNameToScore.put(candidatePlayerName, playerNameToScore.get(candidatePlayerName) + DRAW.getScore());
			}
		}

		int candidateScore = playerNameToScore.get(candidatePlayerName);
		int currentScore = playerNameToScore.get(currentPlayerName);
		if (candidateScore - currentScore > 0 && !degenerated(candidateScore, EVALUATION_GAMES_COUNT * WIN.getScore())) {
			logger.info("Current best player has changed. Score: {} -> {}", currentScore, candidateScore);
			return candidate;
		} else {
			logger.info("current best player has not changed. Score: {} -/> {}", currentScore, candidateScore);
			return current;
		}
	}

	/**
	 * This detects a degenerate case where all games have been won by one player. This happens, I think, when all of
	 * the evaluation games happened to be identical. Unfortunately, I don't know why this happens so often. For now,
	 * let's use this hackish workaround and move on.
	 *
	 * TODO remove when not needed
	 */
	private static boolean degenerated(int score, int perfectScore) {
		return score == 0 || score == perfectScore || score == -perfectScore;
	}

}
