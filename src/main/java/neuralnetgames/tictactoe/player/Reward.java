package neuralnetgames.tictactoe.player;

public enum Reward {

	WIN(1),
	DRAW(0),
	LOSS(-1);

	private final int score;

	private Reward(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}
}
