package neuralnetgames.tictactoe.player;

import static java.lang.String.format;

import neuralnetgames.tictactoe.board.TicTacToeBoard;
import neuralnetgames.tictactoe.board.TicTacToePawn;

public abstract class Player {

	private static int ID = 0;

	protected final TicTacToePawn pawn;
	protected final String name;
	private final int id;

	public Player(TicTacToePawn pawn) {
		this.pawn = pawn;
		ID++;
		id = ID;
		this.name = format("Player %s", ID);
	}

	public Player(TicTacToePawn pawn, String name) {
		this.pawn = pawn;
		ID++;
		id = ID;
		this.name = name;
	}

	public abstract TicTacToeBoard nextMove(TicTacToeBoard currentState);

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public TicTacToePawn getPawn() {
		return pawn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pawn == null) ? 0 : pawn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Player other = (Player) obj;
		if (id != other.id) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (pawn != other.pawn) {
			return false;
		}
		return true;
	}

}
