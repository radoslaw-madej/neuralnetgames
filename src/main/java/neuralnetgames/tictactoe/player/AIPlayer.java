package neuralnetgames.tictactoe.player;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.lang.Math.pow;
import static java.lang.String.format;
import static neuralnetgames.tictactoe.player.AIPlayerMode.DETERMINISTIC;
import static neuralnetgames.tictactoe.player.AIPlayerMode.STOCHASTIC;
import static neuralnetgames.tictactoe.player.Reward.DRAW;
import static neuralnetgames.tictactoe.player.Reward.LOSS;
import static neuralnetgames.tictactoe.player.Reward.WIN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import neuralnetgames.tictactoe.board.TicTacToePawn;
import neuralnetgames.tictactoe.board.TicTacToeBoard;
import neuralnetgames.tictactoe.board.TicTacToeNetAdapter;
import neuralnetgames.tictactoe.montecarlotree.MonteCarloTree;

public class AIPlayer extends Player {

	/**
	 * The lower the temperature gets, the more deterministic the choice becomes. The higher it gets, the more hectic
	 * the choice gets.
	 *
	 * Allowed values expand from 0 (exclusively), to {@link Double#MAX_VALUE} (inclusively).
	 *
	 * The neutral value is 1. It corresponds to choosing with probability exactly proportional to the number of games
	 * from a given tree node.
	 */
	private static final double TEMPERATURE = 1.2;
	private static final int MONTE_CARLO_ROLLOUTS = 50;
	private static final Function<MonteCarloTree, Double> TREE_APPEAL_FUNCTION = treeAppealFunction();
	private static final double EXPLORATION_CONSTANT = 2.0;

	private final TicTacToeNetAdapter net;
	private final Random random;

	private final int boardSize;
	private final int lineLengthToWin;

	public AIPlayer(TicTacToePawn pawn, int boardSize, int lineLengthToWin, Random random) {
		this(pawn, boardSize, lineLengthToWin, random, new TicTacToeNetAdapter(boardSize, random));
	}

	/**
	 * TODO unify the code in this constructor and that one:
	 * {@link #AIPlayer(TicTacToePawn, int, int, Random, TicTacToeNetAdapter)}
	 */
	private AIPlayer(TicTacToePawn pawn, int boardSize, int lineLengthToWin, Random random, TicTacToeNetAdapter net, String name) {
		super(pawn, name);
		this.boardSize = boardSize;
		this.lineLengthToWin = lineLengthToWin;
		this.net = net;
		this.random = random;
	}

	/**
	 * TODO unify the code in this constructor and that one:
	 * {@link #AIPlayer(TicTacToePawn, int, int, Random, TicTacToeNetAdapter, String)}
	 */
	private AIPlayer(TicTacToePawn pawn, int boardSize, int lineLengthToWin, Random random, TicTacToeNetAdapter net) {
		super(pawn);
		this.boardSize = boardSize;
		this.lineLengthToWin = lineLengthToWin;
		this.net = net;
		this.random = random;
	}

	@Override
	public TicTacToeBoard nextMove(TicTacToeBoard currentState) {
		return nextMove(currentState, DETERMINISTIC, newArrayList());
	}

	public TicTacToeBoard nextMoveStochastic(TicTacToeBoard currentState) {
		return nextMove(currentState, STOCHASTIC, newArrayList());
	}

	private TicTacToeBoard nextMove(TicTacToeBoard currentState, AIPlayerMode mode,
			Collection<TrainingDataPoint> trainingDataPointsAccumulator) {

		checkArgument(!currentState.isFinished(), "game has already finished");
		MonteCarloTree tree = generateMonteCarloTree(currentState, net, MONTE_CARLO_ROLLOUTS);
		trainingDataPointsAccumulator.add(trainingDataPoint(currentState, tree, TEMPERATURE));
		MonteCarloTree chosenMove = chooseMove(tree, mode, random, TEMPERATURE);
		return chosenMove.getBoard();
	}

	public void trainOnce(int games, int gamesSample) {
		ArrayList<TrainingDataPoint> allTrainingDataPoints = newArrayList();
		for (int i = 0; i < games; i++) {
			Collection<TrainingDataPoint> trainingDataPoints = selfPlay();
			allTrainingDataPoints.addAll(trainingDataPoints);
		}
		List<TrainingDataPoint> sampledDataPoints = sample(allTrainingDataPoints, gamesSample, random);
		sampledDataPoints.forEach(this::trainNet);
	}

	private List<TrainingDataPoint> sample(ArrayList<TrainingDataPoint> allTrainingDataPoints, int gamesSample,
			Random random) {

		List<TrainingDataPoint> result = newArrayList();
		Set<Integer> sampledIndexes = newHashSet();
		while(sampledIndexes.size() < gamesSample) {
			sampledIndexes.add(random.nextInt(allTrainingDataPoints.size()));
		}
		for (int i = 0; i < gamesSample; i++) {
			if (sampledIndexes.contains(i)) {
				result.add(allTrainingDataPoints.get(i));
			}
		}
		return result;
	}

	private Collection<TrainingDataPoint> selfPlay() {
		TicTacToeBoard board = TicTacToeBoard.emptyBoard(boardSize, lineLengthToWin);
		Collection<TrainingDataPoint> trainingDataPoints = newArrayList();
		while (!board.isFinished()) {
			board = nextMove(board, STOCHASTIC, trainingDataPoints);
		}
		addWinnerInformation(trainingDataPoints, board.getWinner());
		return trainingDataPoints;
	}

	private void addWinnerInformation(Collection<TrainingDataPoint> trainingDataPoints, Optional<TicTacToePawn> winner) {
		if (!winner.isPresent()) {
			trainingDataPoints.forEach(trainingDataPoint -> trainingDataPoint.reward = DRAW.getScore());
		} else {
			TicTacToePawn actualWinner = winner.get();

			// winners
			trainingDataPoints
					.stream()
					.filter(trainingDataPoint -> trainingDataPoint.board.getPawnToPlay() == actualWinner)
					.forEach(trainingDataPoint -> trainingDataPoint.reward = WIN.getScore());

			// losers
			trainingDataPoints
					.stream()
					.filter(trainingDataPoint -> trainingDataPoint.board.getPawnToPlay() != actualWinner)
					.forEach(trainingDataPoint -> trainingDataPoint.reward = LOSS.getScore());
		}
	}

	private void trainNet(TrainingDataPoint trainingDataPoint) {
		net.train(
				trainingDataPoint.board,
				trainingDataPoint.moveToProbability,
				trainingDataPoint.reward);
	}

	public AIPlayer copy() {
		return copy(pawn);
	}

	public AIPlayer copy(TicTacToePawn pawn) {
		return copy(pawn, name);
	}

	public AIPlayer copy(TicTacToePawn pawn, String name) {
		return new AIPlayer(pawn, boardSize, lineLengthToWin, random, net.copy(), name);
	}

	public int getBoardSize() {
		return boardSize;
	}

	public int getLineLengthToWin() {
		return lineLengthToWin;
	}

	private static MonteCarloTree generateMonteCarloTree(TicTacToeBoard currentBoard, TicTacToeNetAdapter net,
			int rolloutsCount) {

		MonteCarloTree root = new MonteCarloTree(currentBoard, net.moveEvaluation(currentBoard));
		for (int i = 0; i < rolloutsCount; i++) {
			expandMonteCarloTree(root, net);
		}
		return root;
	}

	private static void expandMonteCarloTree(MonteCarloTree tree, TicTacToeNetAdapter net) {
		if (tree.getChildren().isEmpty()) {
			TicTacToeBoard currentBoard = tree.getBoard();
			if (!currentBoard.isFinished()) {
				currentBoard.getAvailableMoves().forEach(move -> {
					tree.addChild(move, net.moveProbability(currentBoard, move));
				});
			}
			double treeValueAddend = net.moveEvaluation(currentBoard);
			backpropagate(tree, treeValueAddend);
		} else {
			MonteCarloTree chosenChild = tree.getChildren()
					.stream()
					.max(Comparator.comparing(TREE_APPEAL_FUNCTION))
					.get();

			expandMonteCarloTree(chosenChild, net);
		}
	}

	private static void backpropagate(MonteCarloTree tree, double treeValueAddend) {
		tree.increaseTreeValue(treeValueAddend);
		if (tree.getParent().isPresent()) {
			backpropagate(tree.getParent().get(), treeValueAddend);
		}
	}

	private static Function<MonteCarloTree, Double> treeAppealFunction() {
		return tree -> {
			double result = EXPLORATION_CONSTANT * tree.getPredictedProbability() / (1.0 + (double) tree.getGames());
			if (tree.getGames() != 0) {
				result += tree.meanTreeValue();
			}
			return result;
		};
	}

	private static MonteCarloTree chooseMove(MonteCarloTree tree, AIPlayerMode mode, Random random,
			double temperature) {

		checkArgument(!tree.getChildren().isEmpty(), "no move to choose from");
		switch (mode) {
		case DETERMINISTIC:
			return chooseMoveDeterministically(tree);
		case STOCHASTIC:
			return chooseMoveStochastically(tree, random, temperature);
		}
		throw new IllegalStateException(format("unknown %s value: %s", AIPlayerMode.class, mode));
	}

	private static MonteCarloTree chooseMoveDeterministically(MonteCarloTree treeWithNonEmptyChildren) {
		return treeWithNonEmptyChildren.getChildren()
				.stream()
				.max(Comparator.comparing(MonteCarloTree::getGames))
				.get();
	}

	private static MonteCarloTree chooseMoveStochastically(MonteCarloTree treeWithNonEmptyChildren, Random random,
			double temperature) {

		Map<MonteCarloTree, Double> childToProbability = probabilityDistribution(treeWithNonEmptyChildren, temperature);
		return randomElementFromDistribution(childToProbability, random);
	}

	private static <T> T randomElementFromDistribution(Map<T, Double> distribution, Random random) {
		List<Entry<T, Double>> sortedTreeToProbability = distribution.entrySet()
				.stream()
				.sorted(Comparator.comparing(entry -> entry.getValue()))
				.collect(Collectors.toList());

		double chosenCummulativeProbability = random.nextDouble();
		double cummulativeProbability = 0.0;
		for (Entry<T, Double> treeToProbability : sortedTreeToProbability) {
			cummulativeProbability += treeToProbability.getValue();
			if (chosenCummulativeProbability < cummulativeProbability) {
				return treeToProbability.getKey();
			}
		}
		throw new IllegalStateException("incorrect distribution");
	}

	private static Map<MonteCarloTree, Double> probabilityDistribution(MonteCarloTree treeWithNonEmptyChildren,
			double temperature) {

		double exponent = pow(temperature, -1);

		Map<MonteCarloTree, Double> childToProbabilityNotNormalized = treeWithNonEmptyChildren.getChildren()
				.stream()
				.collect(Collectors.toMap(
						tree -> tree,
						tree -> pow(tree.getGames(), exponent)));

		double normalizationDenominator = childToProbabilityNotNormalized.values()
				.stream()
				.mapToDouble(d -> d)
				.sum();

		Map<MonteCarloTree, Double> childToProbabilityNormalized = childToProbabilityNotNormalized.entrySet()
				.stream()
				.collect(Collectors.toMap(
						entry -> entry.getKey(),
						entry -> entry.getValue() / normalizationDenominator));

		return childToProbabilityNormalized;
	}

	private static TrainingDataPoint trainingDataPoint(TicTacToeBoard board, MonteCarloTree tree, double temperature) {
		TrainingDataPoint result = new TrainingDataPoint();
		result.board = board;
		result.moveToProbability = moveToProbability(tree, temperature);
		return result;
	}

	private static Map<TicTacToeBoard, Double> moveToProbability(MonteCarloTree tree, double temperature) {
		if (tree.getChildren().isEmpty()) {
			return newHashMap();
		}
		return probabilityDistribution(tree, temperature).entrySet()
				.stream()
				.collect(Collectors.toMap(entry -> entry.getKey().getBoard(), entry -> entry.getValue()));
	}

	private static class TrainingDataPoint {
		private TicTacToeBoard board;
		private Map<TicTacToeBoard, Double> moveToProbability;
		private int reward;
	}

}
