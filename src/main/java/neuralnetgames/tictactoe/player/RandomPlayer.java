package neuralnetgames.tictactoe.player;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Random;

import org.pcollections.PSet;

import neuralnetgames.tictactoe.board.TicTacToePawn;
import neuralnetgames.tictactoe.board.TicTacToeBoard;

public class RandomPlayer extends Player {

	private final Random random;

	public RandomPlayer(TicTacToePawn pawn, Random random) {
		super(pawn);
		this.random = random;
	}

	public RandomPlayer(TicTacToePawn pawn, String name, Random random) {
		super(pawn, name);
		this.random = random;
	}

	/**
	 * TODO This should be an ordered collection to make the iteration order meaningful.
	 */
	@Override
	public TicTacToeBoard nextMove(TicTacToeBoard currentState) {
		PSet<TicTacToeBoard> availableMoves = currentState.getAvailableMoves();
		checkArgument(!availableMoves.isEmpty(), "no available moves");
		int moveCount = availableMoves.size();
		int chosenMove = random.nextInt(moveCount);
		int i = 0;
		for (TicTacToeBoard move : availableMoves) {
			if (i == chosenMove) {
				return move;
			}
			i++;
		}
		throw new IllegalStateException("a move must have been found");
	}

}
