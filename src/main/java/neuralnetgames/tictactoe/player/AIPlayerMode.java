package neuralnetgames.tictactoe.player;

public enum AIPlayerMode {

	DETERMINISTIC, // for competitive play
	STOCHASTIC; // for training

}
