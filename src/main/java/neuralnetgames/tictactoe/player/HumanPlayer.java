package neuralnetgames.tictactoe.player;

import static java.lang.String.format;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import neuralnetgames.tictactoe.board.Coordinates;
import neuralnetgames.tictactoe.board.TicTacToePawn;
import neuralnetgames.tictactoe.board.TicTacToeBoard;

public class HumanPlayer extends Player {

	//TODO System.in should not be used here, but a stream redirection.
	private BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));

	public HumanPlayer(TicTacToePawn pawn) {
		super(pawn);
	}

	@Override
	public TicTacToeBoard nextMove(TicTacToeBoard currentState) {
		//TODO System.out should not be used here, but a stream redirection.
		System.out.println("Please select your next move: ");
		try {
			int selectedRow = Integer.parseInt(this.inputReader.readLine());
			int selectedColumn = Integer.parseInt(this.inputReader.readLine());

			for (TicTacToeBoard availableMove : currentState.getAvailableMoves()) {
				Coordinates availableLastMove = availableMove.getLastMove().get();
				if (availableLastMove.getRow() == selectedRow && availableLastMove.getColumn() == selectedColumn) {
					return availableMove;
				}
			}
			throw new IllegalArgumentException(format("invalid move: [%s, %s] for pawn: %s",
					selectedRow, selectedColumn, pawn));

		} catch (NumberFormatException | IOException e) {
			// TODO: Add exception handling
			throw new RuntimeException(e);
		}
	}

}
