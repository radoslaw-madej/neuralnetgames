package neuralnetgames.tictactoe.board;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static neuralnetgames.tictactoe.board.TicTacToePawn.CROSS;
import static neuralnetgames.tictactoe.board.TicTacToePawn.NOUGHT;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class TicTacToeNetAdapterUnitTest {

	private final Random randomMock = newRandomMock();


	@Test
	public void testInputSignal() {
		TicTacToeNetAdapter adapter = new TicTacToeNetAdapter(3, randomMock);

		TicTacToeBoard board = TicTacToeBoard.emptyBoard(3, 3)
				.play(CROSS, 0, 2)
				.play(NOUGHT, 0, 0)
				.play(CROSS, 1, 1)
				.play(NOUGHT, 0, 1)
				.play(CROSS, 2, 2)
				.play(NOUGHT, 2, 0)
				.play(CROSS, 1, 0)
				.play(NOUGHT, 2, 1);

		ArrayList<Double> actualSignal = adapter.inputSignal(board);
		ArrayList<Double> expectedSignal = newArrayList(
				// crosses
				0.0, 0.0, 1.0,
				1.0, 1.0, 0.0,
				0.0, 0.0, 1.0,

				// noughts
				1.0, 1.0, 0.0,
				0.0, 0.0, 0.0,
				1.0, 1.0, 0.0,

				// which move is it?
				1.0);

		assertEquals("input signal", expectedSignal, actualSignal);
	}

	@Test
	public void testExpectedOutputPolicySignal() {
		TicTacToeNetAdapter adapter = new TicTacToeNetAdapter(2, randomMock);
		TicTacToeBoard board = TicTacToeBoard.emptyBoard(2, 2)
				.play(CROSS, 0, 0)
				.play(NOUGHT, 0, 1);

		Map<TicTacToeBoard, Double> moveToProbability = newHashMap();
		moveToProbability.put(board.play(CROSS, 1, 0), 0.3);
		moveToProbability.put(board.play(CROSS, 1, 1), 0.7);

		ArrayList<Double> actualExpectedOutputPolicySignal = adapter.expectedOutputPolicySignal(moveToProbability);
		ArrayList<Double> expectedExpectedOutputPolicySignal = newArrayList(0.0, 0.0, 0.3, 0.7);

		assertEquals("expected output policy signal",
				expectedExpectedOutputPolicySignal, actualExpectedOutputPolicySignal);
	}

	public Object[] expectedOutputEvaluationSignalDataProvider() {
		return new Object[] {
				new Object[] { 1, 1.0 },
				new Object[] { 0, 0.5 },
				new Object[] { -1, 0.0 }
		};
	}

	@Test
	@Parameters(method = "expectedOutputEvaluationSignalDataProvider")
	public void testExpectedOutputEvaluationSignal(Integer reward, Double expectedEvaluation) {
		TicTacToeNetAdapter adapter = new TicTacToeNetAdapter(2, randomMock);
		ArrayList<Double> actualExpectedOutputEvaluationSignal = adapter.expectedOutputEvaluationSignal(reward);
		ArrayList<Double> expectedExpectedOutputEvaluationSignal = newArrayList(expectedEvaluation);
		assertEquals("expected output evaluation signal",
				expectedExpectedOutputEvaluationSignal, actualExpectedOutputEvaluationSignal);
	}

	private static Random newRandomMock() {
		Random result = mock(Random.class);
		when(result.nextDouble()).thenReturn(1.0);
		return result;
	}

}
