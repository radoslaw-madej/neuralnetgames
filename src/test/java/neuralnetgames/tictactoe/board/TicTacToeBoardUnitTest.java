package neuralnetgames.tictactoe.board;

import static com.google.common.collect.Sets.newHashSet;
import static java.lang.String.format;
import static neuralnetgames.tictactoe.board.TicTacToePawn.CROSS;
import static neuralnetgames.tictactoe.board.TicTacToePawn.NOUGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class TicTacToeBoardUnitTest {

	private static final TicTacToeBoard EMPTY = TicTacToeBoard.emptyBoard(3, 3);

	private static final TicTacToeBoard BOARD = EMPTY
			.play(CROSS, 0, 0)
			.play(NOUGHT, 0, 1)
			.play(CROSS, 1, 1)
			.play(NOUGHT, 0, 2);

	private static final TicTacToeBoard BOARD_WITH_DIFFERENT_ORDER_OF_PLAY = EMPTY
			.play(CROSS, 1, 1)
			.play(NOUGHT, 0, 2)
			.play(CROSS, 0, 0)
			.play(NOUGHT, 0, 1);

	private static final TicTacToeBoard YET_ANOTHER_BOARD_WITH_DIFFERENT_ORDER_OF_PLAY = EMPTY
			.play(CROSS, 1, 1)
			.play(NOUGHT, 0, 1)
			.play(CROSS, 0, 0)
			.play(NOUGHT, 0, 2);

	private static final TicTacToeBoard BOARD_WON_BY_CROSSES = EMPTY
			.play(CROSS, 0, 0)
			.play(NOUGHT, 0, 1)
			.play(CROSS, 1, 1)
			.play(NOUGHT, 0, 2)
			.play(CROSS, 2, 2);

	private static final TicTacToeBoard TWO_LAST_MOVES_AVAILABLE_BOARD = EMPTY
			.play(CROSS, 0, 0)
			.play(NOUGHT, 1, 0)
			.play(CROSS, 0, 1)
			.play(NOUGHT, 1, 1)
			.play(CROSS, 2, 0)
			.play(NOUGHT, 2, 1)
			.play(CROSS, 1, 2);

	private static final TicTacToeBoard LAST_MOVE_AVAILABLE_BOARD = TWO_LAST_MOVES_AVAILABLE_BOARD
			.play(NOUGHT, 0, 2);

	private static final TicTacToeBoard DRAW_BOARD = LAST_MOVE_AVAILABLE_BOARD
			.play(CROSS, 2, 2);


	public Object[] finishedDataProvider() {
		return new Object[] {
				// empty
				new Object[] { EMPTY, false },
				// crosses won
				new Object[] { BOARD_WON_BY_CROSSES, true },
				// unfinished board
				new Object[] { TWO_LAST_MOVES_AVAILABLE_BOARD, false },
				// draw board
				new Object[] { DRAW_BOARD, true }
		};
	}

	@Test
	@Parameters(method = "finishedDataProvider")
	public void testFinished(TicTacToeBoard board, Boolean finished) {
		assertEquals(format("board %s finished", board), finished, board.isFinished());
	}

	public Object[] winnerDataProvider() {
		return new Object[] {
				// empty
				new Object[] { EMPTY, Optional.empty() },
				// crosses won
				new Object[] { BOARD_WON_BY_CROSSES, Optional.of(CROSS) },
				// unfinished board
				new Object[] { TWO_LAST_MOVES_AVAILABLE_BOARD, Optional.empty() },
				// draw board
				new Object[] { DRAW_BOARD, Optional.empty() }
		};
	}

	@Test
	@Parameters(method = "winnerDataProvider")
	public void testWinner(TicTacToeBoard board, Optional<TicTacToePawn> expectedWinner) {
		assertEquals(format("winner for board: %s", board), expectedWinner, board.getWinner());
	}

	public Object[] availableMovesDataProvider() {
		return new Object[] {
				// no available moves due to existing winner
				new Object[] { BOARD_WON_BY_CROSSES, newHashSet() },
				// no available moves due to draw
				new Object[] { DRAW_BOARD, newHashSet() },
				// last move available
				new Object[] { LAST_MOVE_AVAILABLE_BOARD, newHashSet(DRAW_BOARD) },
				// two last moves available
				new Object[] { TWO_LAST_MOVES_AVAILABLE_BOARD, newHashSet(
						TWO_LAST_MOVES_AVAILABLE_BOARD.play(NOUGHT, 2, 2),
						TWO_LAST_MOVES_AVAILABLE_BOARD.play(NOUGHT, 0, 2)) }
		};
	}

	@Test
	@Parameters(method = "availableMovesDataProvider")
	public void testAvailableMoves(TicTacToeBoard currentBoard, Set<TicTacToeBoard> expectedAvailableMoves) {
		assertEquals(format("available moves for board: %s", currentBoard),
				expectedAvailableMoves,
				currentBoard.getAvailableMoves());
	}

	@Test
	public void testEmptyBoardIsEmpty() {
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 3; column++) {
				assertNull(format("field at row: %s, column: %s", row, column), EMPTY.pawnAt(row, column));
			}
		}
	}

	@Test
	public void testPlayMakesRequestedMove() {
		// given
		TicTacToeBoard board = EMPTY;

		// when
		board = board.play(CROSS, 0, 2);
		board = board.play(NOUGHT, 1, 1);
		board = board.play(CROSS, 1, 2);

		// then
		assertEquals(format("pawn at row = %s, column = %s", 0, 2), CROSS, board.pawnAt(0, 2));
		assertEquals(format("pawn at row = %s, column = %s", 1, 1), NOUGHT, board.pawnAt(1, 1));
		assertEquals(format("pawn at row = %s, column = %s", 1, 2), CROSS, board.pawnAt(1, 2));
	}

	@Test
	public void testHashCode() {
		// when
		assertEquals("boards", BOARD, BOARD_WITH_DIFFERENT_ORDER_OF_PLAY);

		// then
		assertEquals("boards' hashCodes", BOARD.hashCode(), BOARD_WITH_DIFFERENT_ORDER_OF_PLAY.hashCode());
	}

	public Object[] equalsDataProvider() {
		return new Object[] {
				new Object[] { EMPTY, null, false },
				new Object[] { EMPTY, BOARD, false },

				// reflexive
				new Object[] { EMPTY, EMPTY, true },

				// symmetric
				new Object[] { BOARD, BOARD_WITH_DIFFERENT_ORDER_OF_PLAY, true },
				new Object[] { BOARD_WITH_DIFFERENT_ORDER_OF_PLAY, BOARD, true },

				// transitive
				new Object[] { BOARD, BOARD_WITH_DIFFERENT_ORDER_OF_PLAY, true },
				new Object[] { BOARD_WITH_DIFFERENT_ORDER_OF_PLAY, YET_ANOTHER_BOARD_WITH_DIFFERENT_ORDER_OF_PLAY, true },
				new Object[] { BOARD, YET_ANOTHER_BOARD_WITH_DIFFERENT_ORDER_OF_PLAY, true },
		};
	}

	@Test
	@Parameters(method = "equalsDataProvider")
	public void testEquals(TicTacToeBoard first, TicTacToeBoard second, boolean shouldBeEqual) {
		if (shouldBeEqual) { // when
			assertTrue(format("%s and %s are equal", first, second), first.equals(second)); // then
		} else { // when
			assertFalse(format("%s and %s are equal", first, second), first.equals(second)); // then
		}
	}

}
